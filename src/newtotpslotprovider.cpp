/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "newtotpslotprovider.h"
#include "duplicateslotexception.h"
#include "noemptyslotexception.h"
#include "nitrokeybase.h"

#include <algorithm>
#include <array>
#include <QDebug>

NewTOTPSlotProvider::NewTOTPSlotProvider()
{
}

TOTPSlot NewTOTPSlotProvider::findFreeSlot(const QList<TOTPSlot> &slotsList, const std::string &slotName)
{
    using Array = std::array <bool, NitrokeyBase::MAX_SLOT_IDX>;
    Array occupiedSlots = {false};

    for (const auto &slot : slotsList) {
        if (occupiedSlots.at(slot.slotNumber()) == false) {
            occupiedSlots.at(slot.slotNumber()) = true;
        } else {
            throw DuplicateSlotException("Duplicated slot");
        }
    }

    auto idx = std::find(std::begin(occupiedSlots), std::end(occupiedSlots), false);

    if (std::end(occupiedSlots) == idx) {
        throw NoEmptySlotException("Empty slot not found");
    }

    Array::size_type slotNumber = idx - std::begin(occupiedSlots);

    return TOTPSlot(slotName, slotNumber);
}
